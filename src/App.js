import React, { useState } from 'react';
function App () {


    const [data, setData] = useState("No data provided")

    const handleNameChange = (event) =>  setData(event.target.value)

    return (
      <div className="App">
        <input name="data" onChange={handleNameChange} value={data} />

        <h1>
          You typed:
          {data}
        </h1>
      </div>
    );
  }

export default App;
